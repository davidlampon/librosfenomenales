//
//  ViewController.m
//  LibrosFenomenales
//
//  Created by David Lampon Diestre on 26/10/13.
//  Copyright (c) 2013 David Lampon Diestre. All rights reserved.
//

#import "ViewController.h"
#import "InformacionLibroViewController.h"

@interface ViewController ()

@property NSMutableArray *libros;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _libros = [[NSMutableArray alloc] init];
    LibroFenomenal *libro = [[LibroFenomenal alloc] init];
    
    // Primer libro
    libro.nombre = @"Don Quijote de la Mancha";
    libro.autor = @"Miguel de Cervantes";
    libro.publicado = @"1605";
    libro.genero = @"Parodia";
    libro.portada = [UIImage imageNamed:@"portadaQuijote.jpg"];
    libro.argumento = @"Don Quijote de la Mancha es una novela escrita por el español Miguel de Cervantes Saavedra. Publicada su primera parte con el título de El ingenioso hidalgo don Quijote de la Mancha a comienzos de 1605, es una de las obras más destacadas de la literatura española y la literatura universal, y una de las más traducidas. En 1615 aparecería la segunda parte del Quijote de Cervantes con el título de El ingenioso caballero don Quijote de la Mancha.";
    [_libros addObject:libro];
    
    // Segundo libro
    libro = [[LibroFenomenal alloc] init];
    libro.nombre = @"Javi López y la cocinera fenomenal";
    libro.autor = @"Juan K. Hatin'";
    libro.publicado = @"2013";
    libro.genero = @"Fantasía";
    libro.portada = [UIImage imageNamed:@"portadaJavi.png"];
    [_libros addObject:libro];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Revisar lo que have cada funcion
-(CeldaFenomenal *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CeldaFenomenal *celda = [tableView dequeueReusableCellWithIdentifier:@"Celda"];
    LibroFenomenal *libro = [_libros objectAtIndex:indexPath.row];
    
    celda.nombreLabel.text = libro.nombre;
    celda.autorLabel.text = libro.autor;
    celda.portadaImageView.image = libro.portada;
    
    return celda;
}

// Número de filas en la sección
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_libros count];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Deseleccionar la celda pulsada
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    // Crear una instancia de la vista de detalle llamada InformacionLibroViewController
    InformacionLibroViewController *informacionLibroViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"InformacionLibroViewController"];
    
    // Botón de navegación
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:informacionLibroViewController];
    
    // Realizar una animacion al cambiar de vista
    [self presentViewController:navigationController animated:YES completion:nil];
    
    // Le pasammos el libro seleccionado al otro VC    
    informacionLibroViewController.libroSeleccionado = [_libros objectAtIndex:indexPath.row];
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_libros removeObjectAtIndex:indexPath.row];
    [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"segue"])
    {
        UINavigationController *navigationController = (UINavigationController *)segue.destinationViewController;
        NuevoLibroViewController *nuevoLibroViewController = (NuevoLibroViewController *)navigationController.topViewController;
        nuevoLibroViewController.libros = _libros;
        nuevoLibroViewController.delegate = self;
    }
}

- (void)nuevoLibroViewController:(NuevoLibroViewController *)controller haCreadoLibro:(LibroFenomenal *)nuevoLibro
{
    int nuevaFila = [_libros count];
    [_libros addObject:nuevoLibro];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:nuevaFila inSection:0];
    NSArray *indexPaths = [NSArray arrayWithObject:indexPath];
    [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
