//
//  CeldaFenomenal.m
//  LibrosFenomenales
//
//  Created by David Lampon Diestre on 28/10/13.
//  Copyright (c) 2013 David Lampon Diestre. All rights reserved.
//

#import "CeldaFenomenal.h"

@implementation CeldaFenomenal

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
