//
//  ViewController.h
//  LibrosFenomenales
//
//  Created by David Lampon Diestre on 26/10/13.
//  Copyright (c) 2013 David Lampon Diestre. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NuevoLibroViewController.h"
#import "CeldaFenomenal.h"

@interface ViewController : UITableViewController <NuevoLibroViewControllerDelegate>

@end
