//
//  CeldaFenomenal.h
//  LibrosFenomenales
//
//  Created by David Lampon Diestre on 28/10/13.
//  Copyright (c) 2013 David Lampon Diestre. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CeldaFenomenal : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nombreLabel;
@property (weak, nonatomic) IBOutlet UILabel *autorLabel;
@property (weak, nonatomic) IBOutlet UIImageView *portadaImageView;

@end
