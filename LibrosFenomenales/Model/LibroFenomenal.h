//
//  LibroFenomenal.h
//  LibrosFenomenales
//
//  Created by David Lampon Diestre on 26/10/13.
//  Copyright (c) 2013 David Lampon Diestre. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LibroFenomenal : NSObject

@property (nonatomic) NSString *nombre;
@property (nonatomic) NSString *autor;
@property (nonatomic) NSString *publicado;
@property (nonatomic) UIImage *portada;
@property (nonatomic) NSString *genero;
@property (nonatomic) NSString *argumento;

@end
