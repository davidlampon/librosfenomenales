//
//  NuevoLibroViewController.h
//  LibrosFenomenales
//
//  Created by David Lampon Diestre on 26/10/13.
//  Copyright (c) 2013 David Lampon Diestre. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LibroFenomenal.h"

@class NuevoLibroViewController;

@protocol NuevoLibroViewControllerDelegate <NSObject>;

- (void)nuevoLibroViewController:(NuevoLibroViewController *)controller haCreadoLibro:(LibroFenomenal *)nuevoLibro;

@end

@interface NuevoLibroViewController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (weak, nonatomic) IBOutlet UITextField *nombreTextField;
@property (weak, nonatomic) IBOutlet UITextField *autorTextField;
@property (weak, nonatomic) IBOutlet UITextField *publicadoTextField;
@property (weak, nonatomic) IBOutlet UITextField *generoTextField;
@property (weak, nonatomic) IBOutlet UITextView *argumentoTextView;
@property (weak, nonatomic) IBOutlet UIImageView *portadaImageView;

// Creación de la variable para designar el delegate
@property (weak, nonatomic) id <NuevoLibroViewControllerDelegate> delegate;

// Almacenaje temporal de los libros añadidos
@property NSMutableArray *libros;

@end
