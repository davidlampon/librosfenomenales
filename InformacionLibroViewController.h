//
//  InformacionLibroViewController.h
//  LibrosFenomenales
//
//  Created by David Lampon Diestre on 26/10/13.
//  Copyright (c) 2013 David Lampon Diestre. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LibroFenomenal.h"

@interface InformacionLibroViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *nombreLabel;
@property (weak, nonatomic) IBOutlet UILabel *autorLabel;
@property (weak, nonatomic) IBOutlet UILabel *publicadoLabel;
@property (weak, nonatomic) IBOutlet UILabel *generoLabel;
@property (weak, nonatomic) IBOutlet UITextView *argumentoTextView;
@property (weak, nonatomic) IBOutlet UIImageView *portadaImageView;

@property LibroFenomenal *libroSeleccionado;
@end
